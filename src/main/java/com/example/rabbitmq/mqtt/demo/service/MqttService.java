package com.example.rabbitmq.mqtt.demo.service;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.PostConstruct;

@Service
public class MqttService {

    private MqttClient client = null;
    private String mqttIpAddress = "localhost";
    private String mqttPort = "1883";
    Logger LOG = LoggerFactory.getLogger(getClass());

    public void publish(String topic, String content) {

        MqttMessage message = new MqttMessage();
        message.setPayload(content.getBytes());
        message.setQos(2);

        try {
            if (client.isConnected()) {
                LOG.info("Connection Status: " + client.isConnected());
            }
            client.publish(topic, message);
        } catch (MqttPersistenceException e) {
            e.printStackTrace();
        } catch (MqttException e) {
            e.printStackTrace();
        } catch(Exception e) {
            e.printStackTrace();
        }

    }

    @PostConstruct
    public void startMqtt() {
        MemoryPersistence persistence = new MemoryPersistence();
        try {
            client = new MqttClient("tcp://" + mqttIpAddress + ":" + mqttPort,
                    MqttClient.generateClientId(), persistence);
        } catch (MqttException e1) {
            e1.printStackTrace();
        }

        MqttConnectOptions connectOptions = new MqttConnectOptions();
        connectOptions.setCleanSession(true);
        connectOptions.setMaxInflight(3000);
        connectOptions.setAutomaticReconnect(true);

        try {
            IMqttToken mqttConnectionToken = client.connectWithResult(connectOptions);
            LOG.info("Connection Status :" + mqttConnectionToken.isComplete());
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

}
