package com.example.rabbitmq.mqtt.demo.controller;

import com.example.rabbitmq.mqtt.demo.service.MqttService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MqttController {

    @Autowired
    private MqttService mqttService;

    @GetMapping("/publish")
    public String publish() {
        mqttService.publish("bindingKey", "Test Message");
        return "Success";
    }
}
